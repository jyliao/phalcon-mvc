<?php
$loader = new \Phalcon\Loader();
$loader->registerDirs(
    array(
        $config->application->modelsDir,
        $config->application->index->controllersDir,
        $config->application->admin->controllersDir,
        $config->application->extensionsDir,
        $config->application->vendorsDir,
        $config->application->wechatDir,
        $config->application->qrcodeDir,
    )
)->registerNamespaces(array(
    'Index\Controllers' => $config->application->index->controllersDir,
    'Admin\Controllers' => $config->application->admin->controllersDir,
    'Ext' => $config->application->extensionsDir,
    $config->application->vendorsDir,
))->register();
$loader->register();