<?php
$router = new Phalcon\Mvc\Router();
$router->add("/:controller/:action/:params",array(
    'namespace'     => 'Index\Controllers',
    'controller'    => 1,
    'action'        => 2,
    'params'        => 3
));
$router->add("/:controller",array(
    'namespace'     => 'Index\Controllers',
    'controller'    => 1
));

$router->add('/admin/:controller/:action/:params', array(
    'namespace' => 'Admin\Controllers',
    'controller' => 1,
    'action' => 2,
    'params' => 3,
));
$router->add('/admin/:controller', array(
    'namespace' => 'Admin\Controllers',
    'controller' => 1
));
$router->add('/admin', array(
    'namespace' => 'Admin\Controllers',
));
return $router;