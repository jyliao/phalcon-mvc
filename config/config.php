<?php
return new \Phalcon\Config(array(
    'debug' => true,
    'domain' => '', //访问域名
    'database' => array(
        'adapter' => 'Mysql',
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => 'root',
        'port' => 3306,
        'dbname' => 'test',
        'charset' => 'UTF8',
        'persistent' => false,
    ),
    'dbWrite' => array(
        'adapter' => 'Mysql',
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => 'root',
        'port' => 3306,
        'dbname' => 'test',
        'charset' => 'UTF8',
        'persistent' => false,
    ),
    'memcache' => array(
        'host' => '127.0.0.1',
        'port' => 31311
    ),
    'redis' => array(
        'host' => '127.0.0.1',
        'port' => 6379,
        'auth' => '',
        'persistent' => false
    ),
    /*'mongo' => array(
        'host' => '127.0.0.1',
        'port' => '',
        'user' => '',
        'pwd' => '',
        'dbname' => 'hp_meeting'
    ),*/
    'application' => array(
        'modelsDir' => ROOT . '/apps/models/',
        'vendorsDir' => ROOT . '/apps/vendors/',
        'extensionsDir' => ROOT . '/apps/extensions/',
        'wechatDir' => ROOT . '/apps/extensions/WeChat/',
        'qrcodeDir' => ROOT . '/apps/extensions/QRCode/',
        'cacheDir' => ROOT . '/caches/',
        'baseUri' => '/',
        'basePath' => '/',//index.php?_url=/
        'upfileDir' => ROOT . '/web/assets/upload/',
        'upfilePath' => '/assets/upload/',
        'upfileMaxSize' => 10485760, // 10M
        'publicUrl' => '',
        'cryptSalt' => '123$%DaD#$$$_+*7jd',
        'index' => array(
            'controllersDir' => ROOT . '/apps/controllers/index/',
            'viewsDir' => ROOT . '/apps/views/index/',
            'baseUri' => '/',
            'staticUri' => '/assets/index/',
            'staticDir' => ROOT . '/assets/index/',
        ),
        'admin' => array(
            'controllersDir' => ROOT . '/apps/controllers/admin',
            'viewsDir' => ROOT . '/apps/views/admin/',
            'baseUri' => '/admin/',
            'staticUri' => '/assets/system/',
            'staticDir' => ROOT . '/assets/admin/',
        ),
    ),
    'Wechat' => array(
        'KeywordTrigger' => array(
            0 => '全字匹配',
            1 => '存在匹配'
        ),
        'KeywordType' => array(
            'text' => '文字消息',
            'image' => '图片消息',
            'news' => '图文消息'
        ),
    ),
	//微信开放平台
	'OpenWechat' => array(
		'appId' => '',
		'secret' => '',
		'token' => '',
		'encodingAESKey' => '',
		'debug' => false,
		'logcallback' => false
	),
    //服务号
    'FWWeChat' => array(
        'appId' => "",
        'secret' => "",
        'token' => '',
        'encodingAESKey' => '',
        'debug' => false,
        'logcallback' => false
    ),
    //服务号支付
    'PAYWeChat' => array(
        'mch_id' => 0,
        'appId' => '',
        'secret' => "",
        'key' => '',
        'sslCert' => ROOT . '/config/WeChatCert/apiclient_cert.pem',
        'sslKey' => ROOT . '/config/WeChatCert/apiclient_key.pem',
        'nonceStr' => '!mdo8512#DDSE98mn()&321',
        'notify_url' => 'http://domain/pay/pay/weChatNotify'
    ),
	//新浪微博
    'sinaWeibo' => array(
        'appKey' => 0,
        'appSecret' => '',
        'debug' => false
    ),
	//QQ
    'QQ' => array(
        'appId' => 0,
        'appKey' => '',
        'debug' => false
    ),
	//支付宝
    'Alipay' => array(
        //合作身份者ID，签约账号，以2088开头由16位纯数字组成的字符串，查看地址：https://b.alipay.com/order/pidAndKey.htm
        'partner' => '',
        //收款支付宝账号，以2088开头由16位纯数字组成的字符串，一般情况下收款账号就是签约账号
        'seller_id' => '',
        // MD5密钥，安全检验码，由数字和字母组成的32位字符串，查看地址：https://b.alipay.com/order/pidAndKey.htm
        'key' => '',
        // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
        'notify_url' => "http://domain/pay/alipay/notify",
        // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
        'return_url' => "http://domain/pay/alipay/return",
        //签名方式
        'sign_type' => strtoupper('MD5'),
        //字符编码格式 目前支持 gbk 或 utf-8
        'input_charset' => strtolower('utf-8'),
        //ca证书路径地址，用于curl中ssl校验
        //请保证cacert.pem文件在当前文件夹目录中
        'cacert' => ROOT . '/config/alipay_cacert.pem',
        //访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
        'transport' => 'http',
        // 支付类型 ，无需修改
        'payment_type' => "1",
        // 产品类型，无需修改
        'service' => "create_direct_pay_by_user",
        //↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
        //↓↓↓↓↓↓↓↓↓↓ 请在这里配置防钓鱼信息，如果没开通防钓鱼功能，为空即可 ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
        // 防钓鱼时间戳  若要使用请调用类文件submit中的query_timestamp函数
        'anti_phishing_key' => "",
        // 客户端的IP地址 非局域网的外网IP地址，如：221.0.0.1
        'exter_invoke_ip' => "",
    )
));