<?php
//坐标地址获取: http://api.map.baidu.com/lbsapi/getpoint/index.html
try {
    define('STATIC_ROOT', dirname(__FILE__));
    define('ROOT', dirname(STATIC_ROOT));
    $config = include __DIR__ . '/../config/config.php';
    include __DIR__ . '/../config/loader.php';
    include __DIR__ . '/../config/services.php';
    $application = new \Phalcon\Mvc\Application($di);
    echo $application->handle()->getContent();
} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    if($config->debug)
    {
        echo "<br>\n","File = ", $e->getFile(), "<br>\n";
        echo "Line = ", $e->getLine(), "<br>\n";
        echo $e->getTraceAsString(), "<br>\n";
    }
}