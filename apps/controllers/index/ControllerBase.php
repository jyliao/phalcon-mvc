<?php
namespace Index\Controllers;
/**
 * File: ControllerBase.php
 * User: LiaoJiangYi
 * Time: 2016/11/29 14:10
 * QQ:   527532113
 * WeChat: jyliao_vip
 * Mail: jyliao@vip.qq.com
 * @property \Ext\WeChat\Base|\Ext\WeChat\Fw $_FwWeChat
 * @property \Ext\WeChat\Base|\Ext\WeChat\Pay $_PayWeChat
 * @property \Ext\WeChat\Base|\Ext\WeChat\FwOpen $_OpenWeChat
 * @property \Phalcon\Cache\Backend\Redis $redis
 */
class ControllerBase extends \Phalcon\Mvc\Controller
{
    //当前url
    protected $_SelfUr;
    protected $_FwWeChat;
    protected $_PayWeChat;
	protected $_OpenWeChat;
    public function beforeExecuteRoute($dispatcher)
    {
        $this->_SelfUr = $this->request->getScheme().'://'.$this->request->getHttpHost().$this->request->getURI();
        $this->_FwWeChat = new \Ext\WeChat\Fw($this->config->FWWeChat);
        $this->_PayWeChat = new \Ext\WeChat\Pay($this->config->PAYWeChat);
        $this->view->setViewsDir($this->config->application->index->viewsDir);
        $this->url->setBaseUri($this->config->application->index->baseUri);
        $this->url->setStaticBaseUri($this->config->application->index->staticUri);
        $this->tag->setTitle('website');
    }
    public function afterExecuteRoute($dispatcher)
    {
        $this->db->close();
        $this->dbWrite->close();
    }
}