<?php

/**
 * Created by BaseModel.php
 * Author: LiaoJiangYi
 * Date: 2015/8/20 18:01
 */
class BaseModel extends \Phalcon\Mvc\Model
{
    public function initialize()
    {
        parent::initialize();
        $this->setReadConnectionService('db');
        $this->setWriteConnectionService('dbWrite');
    }

    protected function setLog($title, $data='')
    {
        if (is_array($data) || is_object($data)) $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        if (is_array($title) || is_object($title)) $title = json_encode($title, JSON_UNESCAPED_UNICODE);
        if ($title) {
            file_put_contents('model_error.log', date('Y-m-d H:i:s') . ' CLASS:' . __CLASS__ . 'TITLE:' . $title . ' DATA: ' . $data, FILE_APPEND);
        } else {
            file_put_contents('model_error.log', date('Y-m-d H:i:s') . ' CLASS:' . __CLASS__ . ' DATA: ' . $data, FILE_APPEND);
        }
    }
    /**
     * 批量添加操作
    $batch = $model->batch();
    $batch->setRows(['user_id', 'notification_id'])
    ->setValues([
    [23,1],
    [24,1],
    [25,1],
    [26,1],
    [27,1],
    [28,1],
    [29,1],
    [30,1],
    ])->insert();
     * @return \BaseBatch
    */
    public function batch()
    {
        return new BaseBatch($this);
    }
    /**
     * @param $model \Phalcon\Mvc\Model
     * @return array
    */
    public static function getErrorMessages($model)
    {
        $msg = array();
        foreach ($model->getMessages() as $message){
            $msg[] = array(
                'Message' => $message->getMessage(),
                'Field' => $message->getField(),
                'Type' => $message->getType()
            );
        }
        return $msg;
    }
}