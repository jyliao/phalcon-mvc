<?php
/**
 * File: Admin.php
 * User: LiaoJiangYi
 * Time: 2016/12/4 12:02
 * QQ:   527532113
 * WeChat: jyliao_vip
 * Mail: jyliao@vip.qq.com
 */
final class Admin extends BaseModel
{
    public function initialize()
    {
        $this->setSource("admin");
    }
    public function getSource()
    {
        return "admin";
    }
}