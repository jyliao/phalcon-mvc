<?php
namespace Ext\Address;
/**
 * Created by PhpStorm.
 * User: jyliao
 * Date: 2016/12/26
 * Time: 11:56
 */
final class Area
{
    public $data;

    public function __construct()
    {
        $file = __DIR__ . '/area.json';
        $this->data = json_decode(file_get_contents($file),true);
    }

    public function get($code)
    {
        if (!is_numeric($code)) return $code;
        $code = intval($code);
        return !empty($this->data[$code]) ? $this->data[$code]['name'] : '';
    }
}