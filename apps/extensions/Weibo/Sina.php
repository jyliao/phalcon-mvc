<?php
/**
 * File: Sina.php
 * User: LiaoJiangYi
 * Time: 2016/3/1 17:15
 * QQ:   527532113
 * WeChat: jyliao_vip
 * Mail: jyliao@vip.qq.com
 */
namespace Ext\Weibo;
class Sina extends Base
{
    const API_URL_PREFIX = 'https://api.weibo.com';
    const OAUTH2_AUTHORIZE_URL = '/oauth2/authorize';
    const OAUTH2_ACCESS_TOKEN_URL = '/oauth2/access_token';
    const OAUTH2_GET_USER_INFO_URL = '/2/users/show.json';
    //
    private $appKey;
    private $appSecret;
    private $debug;
    public $access_token;
    public $remote_ip;
    public function __construct(\Phalcon\Config $options)
    {
        $this->appKey = $options->appKey;
        $this->appSecret = $options->appSecret;
        $this->debug = $options->debug;
        $this->remote_ip = $_SERVER['REMOTE_ADDR'];
    }
    public function parseHttpRequest(callable $callable, $url, $postOptions = null)
    {return $callable($url,$postOptions);}
    public function getOauth2Url($url)
    {
        return self::API_URL_PREFIX . self::OAUTH2_AUTHORIZE_URL .
        '?client_id='.$this->appKey . '&redirect_uri='.urlencode($url);
    }
    public function getOauth2AccessToken($url)
    {
        $code = !empty($_GET['code']) ? $_GET['code'] : '';
        if(!$code)return false;
        return $this->httpPost(self::API_URL_PREFIX.self::OAUTH2_ACCESS_TOKEN_URL,array(
            'client_id' => $this->appKey,
            'client_secret' => $this->appSecret,
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $url
        ));
    }
    public function getOauth2UserInfo($access_token,$uid)
    {
        if(!$access_token)return false;
        return $this->httpGet(self::API_URL_PREFIX.self::OAUTH2_GET_USER_INFO_URL,array(
            'access_token' => $access_token,
            'uid' => $uid,
        ));
    }
}