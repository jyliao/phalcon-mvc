<?php
/**
 * File: QQ.php
 * User: LiaoJiangYi
 * Time: 2016/3/2 16:29
 * QQ:   527532113
 * WeChat: jyliao_vip
 * Mail: jyliao@vip.qq.com
 */
namespace Ext\QQ;
class QQ extends Base
{
    const API_URL_PREFIX = 'https://graph.qq.com';
    const OAUTH2_AUTHORIZE_URL = '/oauth2.0/authorize';
    const OAUTH2_ACCESS_TOKEN_URL = '/oauth2.0/token';
    const OAUTH2_GET_OPEN_ID_URL = '/oauth2.0/me';
    const OAUTH2_GET_USER_INFO_URL = '/user/get_user_info';
    private $appId;
    private $appKey;
    private $debug;
    private $remote_ip;
    public function __construct(\Phalcon\Config $options)
    {
        $this->appId = $options->appId;
        $this->appKey = $options->appKey;
        $this->debug = $options->debug;
        $this->remote_ip = $_SERVER['REMOTE_ADDR'];
    }
    public function parseHttpRequest(callable $callable, $url, $postOptions = null)
    {return $callable($url,$postOptions);}
    public function getOauth2Url($url,$state='')
    {
        $state = rand(0,100000);
        return self::API_URL_PREFIX . self::OAUTH2_AUTHORIZE_URL .
        '?response_type=code&state='.$state.'&client_id='.$this->appId . '&redirect_uri='.urlencode($url);
    }
    public function getOauth2AccessToken($url)
    {
        $code = !empty($_GET['code']) ? $_GET['code'] : '';
        if(!$code)return false;
        return $this->httpGet(self::API_URL_PREFIX.self::OAUTH2_ACCESS_TOKEN_URL,array(
            'grant_type' => 'authorization_code',
            'client_id' => $this->appId,
            'client_secret' => $this->appKey,
            'code' => $code,
            'redirect_uri' => $url
        ));
    }
    public function getOauth2OpenId($access_token)
    {
        if(!$access_token)return false;
        return $this->httpGet(self::API_URL_PREFIX.self::OAUTH2_GET_OPEN_ID_URL,array(
            'access_token' => $access_token,
        ));
    }
    public function getOauth2UserInfo($access_token,$openId)
    {
        if(!$access_token)return false;
        return $this->httpGet(self::API_URL_PREFIX.self::OAUTH2_GET_USER_INFO_URL,array(
            'access_token' => $access_token,
            'oauth_consumer_key' => $this->appId,
            'openid' => $openId,
        ));
    }
}