<?php
/**
 * Created by QRrsblock.php
 * Author: LiaoJiangYi
 * Date: 2015/10/29 11:28
 */
namespace Ext\QRCode;
class QRrsblock
{
    public $dataLength;
    public $data = array();
    public $eccLength;
    public $ecc = array();

    public function __construct($dl, $data, $el, &$ecc, \Ext\QRCode\QRrsItem $rs)
    {
        $rs->encode_rs_char($data, $ecc);
        $this->dataLength = $dl;
        $this->data = $data;
        $this->eccLength = $el;
        $this->ecc = $ecc;
    }
}