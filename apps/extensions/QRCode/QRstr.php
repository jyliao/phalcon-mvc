<?php
/**
 * Created by QRstr.php
 * Author: LiaoJiangYi
 * Date: 2015/10/29 11:07
 */
namespace Ext\QRCode;
class QRstr
{
    public static function set(&$srctab, $x, $y, $repl, $replLen = false) {
        $srctab[$y] = substr_replace($srctab[$y], ($replLen !== false)?substr($repl,0,$replLen):$repl, $x, ($replLen !== false)?$replLen:strlen($repl));
    }
}